// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue'
import '~/assets/css/animate.css';
import '~/assets/css/bootstrap.css';
import '~/assets/css/color-switcher-design.css';
import '~/assets/css/flaticon.css';
import '~/assets/css/fontawesome-all.css';
import '~/assets/css/hover.css';
import '~/assets/css/jquery.fancybox.min.css';
import '~/assets/css/jquery.touchspin.css';
import '~/assets/css/jquery-ui.css';
import '~/assets/css/magnific-popup.css';
import '~/assets/css/owl.css';
import '~/assets/css/responsive.css';
import '~/assets/css/style.css';

// import '~/assets/js/jquery.js';
// import '~/assets/js/popper.min.js';
// import '~/assets/js/bootstrap.min.js';
// import '~/assets/js/jquery.fancybox.js';
// import '~/assets/js/owl.js';
// import '~/assets/js/nav-tool.js';
// import '~/assets/js/main.js';
// import '~/assets/js/appear.js';
// import '~/assets/js/wow.js';
// import '~/assets/js/mixitup.js';
// import '~/assets/js/validate.js';
// import '~/assets/js/script.js';
// import '~/assets/js/color-settings.js';

export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
}
